﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Rigidbody))]
public class ClickToRoll : MonoBehaviour
{
    public bool rolled = false;

    private void OnMouseDown()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        if (rb != null)
        {
            if(!rolled)
            {
                rb.velocity += new Vector3(0, 6f, 0);
                rb.angularVelocity += new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f));
                rolled = true;
            }
        }
    }
}
