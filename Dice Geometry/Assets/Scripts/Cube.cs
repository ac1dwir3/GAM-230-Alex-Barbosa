using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class Cube : MonoBehaviour
{
    private Mesh mesh;

    private List<Vector3> faceNormals = new List<Vector3>();
    private List<Vector3> faceCenters = new List<Vector3>();

    private GameManager gm;
    private int value = 0;

    private bool added = false;

    private void CreateMesh()
    {
        mesh = new Mesh();

        // Quad lies in x-z plane, facing +y
        List<Vector3> vertices = new List<Vector3>();
        // Looking at the faces from the front (looking toward -y)
        vertices.Add(new Vector3(-1, 0, -1)); // 0 - bottom lower right
        vertices.Add(new Vector3( 1, 0, -1)); // 1 - bottom lower left
        vertices.Add(new Vector3( 1, 0,  1)); // 2 - bottom upper left
        vertices.Add(new Vector3(-1, 0,  1)); // 3 - bottom upper right

        vertices.Add(new Vector3(-1, 2, -1)); // 4 - top lower right
        vertices.Add(new Vector3( 1, 2, -1)); // 5 - top lower left
        vertices.Add(new Vector3( 1, 2,  1)); // 6 - top upper left
        vertices.Add(new Vector3(-1, 2,  1)); // 7 - top upper right

        // Clockwise is the front face winding

        List<int> indices = new List<int>();
        // Square side tris
        // Bottom side
        indices.Add(0);
        indices.Add(1);
        indices.Add(2);

        indices.Add(0);
        indices.Add(2);
        indices.Add(3);
        // Right side
        indices.Add(0);
        indices.Add(3);
        indices.Add(7);

        indices.Add(0);
        indices.Add(7);
        indices.Add(4);
        // Back side
        indices.Add(1);
        indices.Add(0);
        indices.Add(4);

        indices.Add(1);
        indices.Add(4);
        indices.Add(5);
        // Left side
        indices.Add(2);
        indices.Add(1);
        indices.Add(5);

        indices.Add(2);
        indices.Add(5);
        indices.Add(6);
        // Front side
        indices.Add(6);
        indices.Add(7);
        indices.Add(3);

        indices.Add(6);
        indices.Add(3);
        indices.Add(2);
        // Top side
        indices.Add(7);
        indices.Add(6);
        indices.Add(5);

        indices.Add(7);
        indices.Add(5);
        indices.Add(4);


        // Copy the vertices that each index refers to so we can get individual normals for each triangle.  Ugh...
        List<Vector3> actualVertices = new List<Vector3>();
        for (int i = 0; i < indices.Count; ++i)
        {
            actualVertices.Add(vertices[indices[i]]);
        }

        // Reset our indices to go in sequence through all of our new actual vertices.
        // NOTE: This ruins to performance gains from using indices in the first place!
        indices.Clear();
        for (int i = 0; i < actualVertices.Count; ++i)
        {
            indices.Add(i);
        }

        mesh.SetVertices(actualVertices);
        mesh.SetTriangles(indices, 0);

        // Set the UVs / texture coordinates
        List<Vector2> uvs = new List<Vector2>();
        // Square base mapping
        for (int n = 0; n < 6; ++n)
        {
            int col = n % 5;
            int row = n / 5;

            Vector2 uvA = new Vector2(0.0f + (col * 0.2f), (1f - 0.00f) - (row * 0.25f));  // UL
            Vector2 uvB = new Vector2(0.2f + (col * 0.2f), (1f - 0.00f) - (row * 0.25f));  // UR
            Vector2 uvC = new Vector2(0.2f + (col * 0.2f), (1f - 0.25f) - (row * 0.25f));  // LR
            Vector2 uvD = new Vector2(0.0f + (col * 0.2f), (1f - 0.25f) - (row * 0.25f));  // LL

            uvs.Add(uvC);
            uvs.Add(uvD);
            uvs.Add(uvA);

            uvs.Add(uvC);
            uvs.Add(uvA);
            uvs.Add(uvB);
        }
        mesh.SetUVs(0, uvs);

        // Calculate our own normals
        List<Vector3> normals = new List<Vector3>();
        for (int i = 0; i < indices.Count; i += 3)
        {
            int n = i / 3;

            Vector3 a = actualVertices[i + 0];
            Vector3 b = actualVertices[i + 1];
            Vector3 c = actualVertices[i + 2];

            // Two (common) ways to multiply vectors: Dot product and the Cross product
            // Dot product: Tells you how aligned two vectors are.
            // Cross product: Constructs a mutually perpendicular new vector
            Vector3 normal = Vector3.Cross(a - b, a - c).normalized;

            normals.Add(normal);
            normals.Add(normal);
            normals.Add(normal);
        }
        mesh.SetNormals(normals);

        // Let's go through each face (skipping the extra triangle for the square base)
        // Calculate the position of the center and the normal
        for (int n = 1; n < 12; n+=2)
        {
            Vector3 a = actualVertices[n * 3 + 0];
            Vector3 b = actualVertices[n * 3 + 1];
            Vector3 c = actualVertices[n * 3 + 2];

            faceCenters.Add(0.95f * (a + b + c) / 3f);

            Vector3 normal = Vector3.Cross(a - b, a - c).normalized;
            faceNormals.Add(normal);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        CreateMesh();

        MeshFilter mf = GetComponent<MeshFilter>();
        mf.mesh = mesh;

        MeshCollider mc = GetComponent<MeshCollider>();
        mc.sharedMesh = mesh;

        gm = GameManager.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(CheckIfLanded(other));
    }

    IEnumerator CheckIfLanded(Collider ground)
    {
        yield return new WaitForSeconds(2);
        if (ground.CompareTag("Ground"))
            gm.playerPoints += FindFace(this);
    }

    public int FindFace(Cube d6)
    {
        for (int f = 0; f < faceCenters.Count; ++f)
        {
            if (!added)
            {
                Vector3 faceDirection = transform.rotation * faceNormals[f];
                float alignment = Vector3.Dot(Vector3.up, faceDirection);
                if (alignment > 0.7f && gm.roundNumber > 0)
                {
                    value = f + 1;
                    Debug.Log("Landed on " + (f + 1));
                    added = true;
                    break;
                }
            }
        }
        added = false;
        return value;
    }

    void OnDrawGizmos()
    {
        for (int f = 0; f < faceCenters.Count; ++f)
        {
            if (Physics.Raycast(transform.rotation * faceCenters[f] + transform.position, transform.rotation * faceNormals[f], 0.3f))
            {
                Gizmos.color = Color.red;
            }
            else
            {
                Gizmos.color = Color.white;
            }
            Gizmos.DrawRay(transform.rotation * faceCenters[f] + transform.position, transform.rotation * faceNormals[f]);
        }
    }
}