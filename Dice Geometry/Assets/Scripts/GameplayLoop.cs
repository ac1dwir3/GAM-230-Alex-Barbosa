using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayLoop : MonoBehaviour
{
    [SerializeField] private Cube dice1, dice2, dice3;

    private GameManager gm;
    
    int playerHealth, enemyHealth, playerPoints, enemyPoints;

    bool isAttacking, turnTaken;

    Cube[] die = new Cube[3];

    // Start is called before the first frame update
    void Start()
    {
        gm = GameManager.Instance;
        playerHealth = gm.playerHealth;
        enemyHealth = gm.enemyHealth;
        playerPoints = gm.playerPoints;
        enemyPoints = gm.enemyPoints;
        turnTaken = gm.turnTaken;

        die[0] = dice1;
        die[1] = dice2;
        die[2] = dice3;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void CheckForFinish()
    {
        if (playerHealth <= 0)
        {
            //GameManager.Lose();
        }
        else if (enemyHealth <= 0)
        {
            //GameManager.Win();
        }
    }

    public void TakeTurn()
    {
        gm.playerPoints = 0;
        gm.enemyPoints = 0;

        if (!turnTaken && gm.roundNumber > 0)
        {
            turnTaken = true;
            RollDice();
            gm.enemyPoints = Random.Range(3, 18);

            if (gm.attacking)
            {
                StartCoroutine(DoAttack());
            }
            else
            {
                StartCoroutine(DoDefend());
            }
            CheckForFinish();
        }
        NextRound();

    }

    IEnumerator DoAttack()
    {
        yield return new WaitUntil(() => gm.playerPoints >= 3);
        yield return new WaitForSeconds(1);
        playerPoints = gm.playerPoints;
        enemyPoints = gm.enemyPoints;

        playerPoints -= enemyPoints;
        if (playerPoints < 0)
            playerPoints = 0;

        gm.enemyHealth -= playerPoints;
    }

    IEnumerator DoDefend()
    {
        yield return new WaitUntil(() => gm.playerPoints >= 3);
        yield return new WaitForSeconds(1);
        playerPoints = gm.playerPoints;
        enemyPoints = gm.enemyPoints;

        enemyPoints -= playerPoints;
        if (enemyPoints < 0)
            enemyPoints = 0;

        gm.playerHealth -= enemyPoints;
    }

    void NextRound()
    {
        gm.roundNumber++;
        gm.attacking = !gm.attacking;
        turnTaken = false;

    }

    private void RollDice()
    {
        for (int i = 0; i < die.Length; i++)
        {
            bool rolled = false;
            Rigidbody rb = die[i].GetComponent<Rigidbody>();
            if (rb != null)
            {
                if (!rolled)
                {
                    rb.velocity += new Vector3(0, 6f, 0);
                    rb.angularVelocity += new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f));
                    rolled = true;
                }
            }
        }
    }
}
