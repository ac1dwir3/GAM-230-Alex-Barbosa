using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class OctaHedron : MonoBehaviour
{
    private Mesh mesh;

    private List<Vector3> faceNormals = new List<Vector3>();
    private List<Vector3> faceCenters = new List<Vector3>();

    private GameManager gm;
    private int value = 0;

    private bool added = false;

    private void CreateMesh()
    {
        mesh = new Mesh();

        // Quad lies in x-z plane, facing +y
        List<Vector3> vertices = new List<Vector3>();
        // Looking at the faces from the front (looking toward -y)
        vertices.Add(new Vector3( 0, 0,  0)); // 0 - bottom middle point

        vertices.Add(new Vector3(-1, 1, -1)); // 1 - middle lower right
        vertices.Add(new Vector3( 1, 1, -1)); // 2 - middle lower left
        vertices.Add(new Vector3( 1, 1,  1)); // 3 - middle upper left
        vertices.Add(new Vector3(-1, 1,  1)); // 4 - middle upper right

        vertices.Add(new Vector3( 0, 2,  0)); // 5 - top middle point

        // Clockwise is the front face winding

        List<int> indices = new List<int>();
        // Bottom triangle tris
        // Bottom left side
        indices.Add(1);
        indices.Add(0);
        indices.Add(4);
        // Bottom back side
        indices.Add(2);
        indices.Add(0);
        indices.Add(1);
        // Bottom right side
        indices.Add(3);
        indices.Add(0);
        indices.Add(2);
        // Bottom front side
        indices.Add(4);
        indices.Add(0);
        indices.Add(3);
        // Top left side
        indices.Add(4);
        indices.Add(5);
        indices.Add(1);
        // Top back side
        indices.Add(1);
        indices.Add(5);
        indices.Add(2);
        // Top right side
        indices.Add(2);
        indices.Add(5);
        indices.Add(3);
        // Top front side
        indices.Add(3);
        indices.Add(5);
        indices.Add(4);


        // Copy the vertices that each index refers to so we can get individual normals for each triangle.  Ugh...
        List<Vector3> actualVertices = new List<Vector3>();
        for (int i = 0; i < indices.Count; ++i)
        {
            actualVertices.Add(vertices[indices[i]]);
        }

        // Reset our indices to go in sequence through all of our new actual vertices.
        // NOTE: This ruins to performance gains from using indices in the first place!
        indices.Clear();
        for (int i = 0; i < actualVertices.Count; ++i)
        {
            indices.Add(i);
        }

        mesh.SetVertices(actualVertices);
        mesh.SetTriangles(indices, 0);

        // Set the UVs / texture coordinates
        List<Vector2> uvs = new List<Vector2>();
        // Triangle sides mapping
        for (int n = 0; n < 8; ++n)
        {
            int col = n % 5;
            int row = n / 5;

            Vector2 uv0 = new Vector2(0.0f + col * 0.2f, 1f - 0.25f - row * 0.25f);  // left-lower texcoord
            Vector2 uv1 = new Vector2(0.2f + col * 0.2f, 1f - 0.25f - row * 0.25f);  // right-lower texcoord
            Vector2 uv2 = new Vector2(0.1f + col * 0.2f, 1f - 0.00f - row * 0.25f);  // center-top texcoord

            uvs.Add(uv0);
            uvs.Add(uv2);
            uvs.Add(uv1);
        }
        mesh.SetUVs(0, uvs);

        // Calculate our own normals
        List<Vector3> normals = new List<Vector3>();
        for (int i = 0; i < indices.Count; i += 3)
        {
            int n = i / 3;

            Vector3 a = actualVertices[i + 0];
            Vector3 b = actualVertices[i + 1];
            Vector3 c = actualVertices[i + 2];

            // Two (common) ways to multiply vectors: Dot product and the Cross product
            // Dot product: Tells you how aligned two vectors are.
            // Cross product: Constructs a mutually perpendicular new vector
            Vector3 normal = Vector3.Cross(a - b, a - c).normalized;

            normals.Add(normal);
            normals.Add(normal);
            normals.Add(normal);
        }
        mesh.SetNormals(normals);

        // Let's go through each face
        // Calculate the position of the center and the normal
        for (int n = 0; n < 8; ++n)
        {
            Vector3 a = actualVertices[n * 3 + 0];
            Vector3 b = actualVertices[n * 3 + 1];
            Vector3 c = actualVertices[n * 3 + 2];

            faceCenters.Add(0.95f * (a + b + c) / 3f);

            Vector3 normal = Vector3.Cross(a - b, a - c).normalized;
            faceNormals.Add(normal);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        CreateMesh();

        MeshFilter mf = GetComponent<MeshFilter>();
        mf.mesh = mesh;

        MeshCollider mc = GetComponent<MeshCollider>();
        mc.sharedMesh = mesh;

        gm = GameManager.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(CheckIfLanded(other));
    }

    IEnumerator CheckIfLanded(Collider ground)
    {
        yield return new WaitForSeconds(2);
        if (ground.CompareTag("Ground"))
            gm.playerPoints += FindFace(this);
    }

    public int FindFace(OctaHedron d8)
    {
        for (int f = 0; f < faceCenters.Count; ++f)
        {
            if (!added)
            {
                Vector3 faceDirection = transform.rotation * faceNormals[f];
                float alignment = Vector3.Dot(Vector3.up, faceDirection);
                if (alignment > 0.8f)
                {
                    value = f + 1;
                    Debug.Log("Landed on " + (f + 1));
                    added = true;
                    return value;
                }
            }
        }

        return 0;
    }

    void OnDrawGizmos()
    {
        for (int f = 0; f < faceCenters.Count; ++f)
        {
            if (Physics.Raycast(transform.rotation * faceCenters[f] + transform.position, transform.rotation * faceNormals[f], 0.3f))
            {
                Gizmos.color = Color.red;
            }
            else
            {
                Gizmos.color = Color.white;
            }
            Gizmos.DrawRay(transform.rotation * faceCenters[f] + transform.position, transform.rotation * faceNormals[f]);
        }
    }
}