﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quad : MonoBehaviour
{
    private Mesh mesh;

    private void CreateMesh()
    {
        mesh = new Mesh();

        // Quad lies in x-y plane, facing +z
        List<Vector3> vertices = new List<Vector3>();
        // Looking at the faces from the front (looking toward -z)
        vertices.Add(new Vector3(-1, -1, 0));  // 0 - lower right
        vertices.Add(new Vector3(1, -1, 0));  // 1 - lower left
        vertices.Add(new Vector3(1, 1, 0)); // 2 - upper left
        vertices.Add(new Vector3(-1, 1, 0)); // 3 - upper right

        // Clockwise is the front face winding

        mesh.SetVertices(vertices);

        List<int> indices = new List<int>();
        indices.Add(0);
        indices.Add(1);
        indices.Add(2);

        indices.Add(0);
        indices.Add(2);
        indices.Add(3);

        mesh.SetTriangles(indices, 0);

        mesh.RecalculateNormals();
    }

    // Start is called before the first frame update
    void Start()
    {
        CreateMesh();

        MeshFilter mf = GetComponent<MeshFilter>();
        mf.mesh = mesh;
    }
}
