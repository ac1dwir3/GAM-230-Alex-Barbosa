﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void LoadScene(string newScene)
    {
        Debug.Log("Loading Scene...");
        SceneManager.LoadScene(newScene);
    }
}
