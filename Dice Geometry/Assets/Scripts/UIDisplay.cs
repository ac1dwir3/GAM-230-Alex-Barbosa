using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI totalValueText;
    [SerializeField] private TextMeshProUGUI playerHealthText;
    [SerializeField] private TextMeshProUGUI enemyHealthText;
    [SerializeField] private TextMeshProUGUI enemyPointsText;

    GameManager gm;

    // Start is called before the first frame update
    void Start()
    {
        gm = GameManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        //Displays Current Player Health
        if (playerHealthText != null)
            playerHealthText.text = "Remaining Health: " + string.Format("{0:0}", gm.playerHealth);

        //Displays Current Enemy Health
        if (enemyHealthText != null)
            enemyHealthText.text = "Remaining Enemy Health: " + string.Format("{0:0}", gm.enemyHealth);

        //Displays Current Total Player Attack/Defense Points
        if (totalValueText != null)
        {
            if (!gm.attacking)
                totalValueText.text = "Total Attack Points: " + string.Format("{0:0}", gm.playerPoints);
            else
                totalValueText.text = "Total Defense Points: " + string.Format("{0:0}", gm.playerPoints);
        }

        //Displays Current Total Enemy Attack/Defense Points
        if (enemyPointsText != null)
        {
            if (!gm.attacking)
                enemyPointsText.text = "Enemy Defense Points: " + string.Format("{0:0}", gm.enemyPoints);
            else
                enemyPointsText.text = "Enemy Attack Points: " + string.Format("{0:0}", gm.enemyPoints);
        }
    }
}
