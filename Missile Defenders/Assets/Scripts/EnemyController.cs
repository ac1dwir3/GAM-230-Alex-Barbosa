﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    //References Player
    [SerializeField] private GameObject playerPrefab;

    //References A Partical System
    [SerializeField] private ParticleSystem destructionParticles;

    //Variables For Basic Enemy
    public float speed = 2.5f;
    public float health = 2.0f;

    ////Variable To Keep Track How Long The Enemy Has Been Alive
    private float lifeTimeClock = 0.0f;

    void Start()
    {
        //Establishes Target As The Player
        playerPrefab = MissileDefenderLevel.Instance.player.gameObject;
        //Increases The Count Of Enemies Present In The Level
        MissileDefenderLevel.Instance.remainingEnemies++;
    }

    void Update()
    {
        //Draws A Red Line From The Enemy To The Player (Only Visible In Editor)
        Debug.DrawLine(transform.position, playerPrefab.transform.position, Color.red);

        //Moves In The Direction Of The Player
        Vector3 direction = (playerPrefab.transform.position - transform.position).normalized;
        transform.position += speed * direction * Time.deltaTime;

        //Increases Timer For However Long The Enemy Is Alive
        lifeTimeClock += Time.deltaTime;

    }

    //Happens When Enemy's Collider Comes In Contact With A Trigger Object
    private void OnTriggerEnter(Collider other)
    {
        //Checks If Collided With A Projectile
        if (other.CompareTag("PlayerProjectile"))
        {
            health--;

            if(health <= 0)
            {
                //Creates Death Particle Effects
                ParticleSystem particles = Instantiate(destructionParticles, transform.position, Quaternion.identity);

                //Increase Score
                GameManager.Instance.score += (100 - (lifeTimeClock * 1.5f));
                //Lowers Amount Of Enemies Present In The Level
                MissileDefenderLevel.Instance.remainingEnemies--;
                //Destroys Self
                Destroy(gameObject);
                //Destroys The Particle System Object After It Finishes Producing Particles
                Destroy(particles.gameObject, particles.main.startLifetime.Evaluate(0f));
            }
        }
    }
}
