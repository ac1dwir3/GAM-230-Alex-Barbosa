﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    public static GameManager Instance { get { return _instance; } }

    public float score = 0;

    void Awake()
    {
        //Makes Sure There Is Only 1 Instance Of The GameManager Object
        if (_instance == null)
        {
            _instance = this;
            //Makes Sure The GameManager Object Persists Throughout Entire Game
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    public void resetScore()
    {
        score = 0;
    }

    public static void Win()
    {
        SceneManager.LoadScene("WinScreen");
    }

    public static void Lose()
    {
        SceneManager.LoadScene("LoseScreen");
    }
}
