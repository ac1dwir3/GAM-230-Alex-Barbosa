﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MissileDefenderLevel : MonoBehaviour
{
    //Timer Variables
    [SerializeField] private TextMeshProUGUI timerValue;
    private float waveTimer = 1f;
    

    //Variables Needed To Create Waves Of Enemies
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private List<WaveDef> waves = new List<WaveDef>();
    public int remainingEnemies = 0;

    //Reference To Player
    [SerializeField] public PlayerController player;

    //Reference To Score Display
    [SerializeField] private TextMeshProUGUI scoreCounter;

    private static MissileDefenderLevel _instance = null;
    public static MissileDefenderLevel Instance { get { return _instance; } }

    private void Awake()
    {
        //Makes Sure There Is Only 1 Instance Of The MissileDefenderLevel Object
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        GameManager.Instance.resetScore();
    }

    void Update()
    {
        //Text For Timer
        timerValue.text = "Next Wave: " + string.Format("{0:F1}", waveTimer);
        //Text For Score
        scoreCounter.text = "Score: " + string.Format("{0:0}", GameManager.Instance.score);

        //Timer CountDown
        if (waveTimer > 0f)
        {
            //Decreases Timer
            waveTimer -= Time.deltaTime;

            //When Timer Hits 0
            if(waveTimer <= 0f)
            {
                waveTimer = 0f;

                //Checks If There Are More Waves To Be Spawned
                if(waves.Count > 0)
                {
                    //Spawns Next Wave
                    Debug.Log("Spawning new wave of " + waves[0].positions.Count + " enemies!");
                    waves[0].Spawn(enemyPrefab);
                    waves.RemoveAt(0);

                    //Resets Timer
                    waveTimer = 10f;
                }
                //If All Waves Have Been Spawned
                else
                {
                    //Checks If All Enemies Have Been Defeated
                    if(remainingEnemies == 0)
                    {
                        Debug.Log("You Win!");
                        GameManager.Win();
                    }
                }
            }
        }
    }
}
