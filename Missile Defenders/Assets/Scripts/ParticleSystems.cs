﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystems : MonoBehaviour
{
    [SerializeField] private AudioSource audioFile;

    private void Start()
    {
        //Destroys Self After Audio Is Finished
        Destroy(gameObject, audioFile.clip.length);
    }
}
