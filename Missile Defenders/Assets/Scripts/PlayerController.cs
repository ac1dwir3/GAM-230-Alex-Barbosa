﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Variables For Turning
    private float facingDegrees = 0f;
    [SerializeField] private float turnSpeed = 180f;

    //Variables For Shooting
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private float projectileSpeed = 40f;

    //Reference To Camera
    [SerializeField] private Camera cam;

    private void Start()
    {
    }

    void Update()
    {
        //Turns Player When The A Or D Key Is Pressed
        float horiz = Input.GetAxis("Horizontal");
        facingDegrees += horiz * turnSpeed * Time.deltaTime;
        transform.rotation = Quaternion.Euler(0f, facingDegrees, 0f);

        //Shoots A Projectile When The Left Mouse Button Is Clicked Or When The Control Key Is Pressed
        if(Input.GetButtonDown("Fire1"))
        {
            //Tracks Where Your Mouse Is On The Screen
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            //Creates A Projectile
            GameObject projectile = Instantiate(projectilePrefab, ray.origin, transform.rotation);
            //Gets RigidBody Component Of The Projectile
            Rigidbody rb = projectile.GetComponent<Rigidbody>();
            if (rb != null)
            {
                //Changes Projectile's Velocity To Shoot Off In The Direction Of Where The Mouse Clicked
                rb.velocity = projectileSpeed * ray.direction;
            }
        }
    }

    //Happens When Player's Collider Comes In Contact With A Trigger Collider
    private void OnTriggerEnter(Collider other)
    {
        //Checks If Collided With An Enemy
        if(other.CompareTag("Enemy"))
        {
            Debug.Log("You Died");

            Destroy(other.gameObject);

            GameManager.Lose();
        }
    }
}
