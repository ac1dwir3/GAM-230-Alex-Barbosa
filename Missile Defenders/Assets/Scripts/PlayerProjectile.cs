﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile : MonoBehaviour
{
    //References A Particle System
    [SerializeField] private ParticleSystem destructionParticles;

    private void Start()
    {
        //Destroys Self After 5 Seconds
        Destroy(gameObject, 5f);
    }

    //Happens When Projectile's Collider Comes In Contact With A Trigger Object
    private void OnTriggerEnter(Collider other)
    {
        //Creates Particle System
        ParticleSystem particles = Instantiate(destructionParticles, transform.position, Quaternion.identity);
        //Destroys Self
        Destroy(gameObject);
    }
}
