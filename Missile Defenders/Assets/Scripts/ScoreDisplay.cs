﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreCounter;
    void Start()
    {
        GameManager gm = GameManager.Instance;
        float score = gm.score;
        scoreCounter.text = "Score: " + string.Format("{0:0}", score);
    }
}
