﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaveDef
{
    //List Of Positions On The Map
    public List<Vector3> positions = new List<Vector3>();

    //Defines How To Spawn An Enemy
    public void Spawn(GameObject prefab)
    {
        for (int i = 0; i < positions.Count; i++)
        {
            GameObject.Instantiate(prefab, positions[i], Quaternion.identity);
        }
    }
}
