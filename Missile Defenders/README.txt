Button Sounds - UI Sfx
https://assetstore.unity.com/packages/audio/sound-fx/ui-sfx-36989

Explosion/Shooting Sounds - Magic & Melee Customizable Sound Library
https://assetstore.unity.com/packages/audio/sound-fx/weapons/magic-melee-customizable-sound-library-176417

Use A and D keys to rotate camera.
Click a location on the screen to shoot at that location.
Defeat enemies in 2 shots.
DON'T GET HIT!!

Enemies are worth 100 points each.
The longer the enemy lives, the less points you will earn for defeating them.
Defeat enemies as quick as possible to earn the most points.