﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    //---------------References---------------

    [SerializeField] private AudioSource sound;
    [SerializeField] private AudioClip pickupSound;

    //Variables/References For Rotation
    private float spinSpeed = 50f;
    private Vector3 axis = new Vector3(0, 1, 0);

    //Variable/Reference For Value Of Collectible
    [SerializeField] float value;

    //----------------------------------------

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(transform.position, axis, spinSpeed * Time.deltaTime);//Rotates Collectible Smoothly
    }

    private void PickUp()
    {
        sound.clip = pickupSound;//Sets Sound Clip To PickUpSound
        sound.Play();//Plays PickUpSound
        Destroy(gameObject, pickupSound.length);//Destroy Collectible After Sound is Finished Playing
        GameManager.Instance.score += value;//Increase Score

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))//If Player Touches The Collectible
        {
            PickUp();
            Debug.Log("10 Points Earned");
        }
    }
}
