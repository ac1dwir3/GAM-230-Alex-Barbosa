﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{
    //---------------References---------------

    //Variable/Reference For Scene Loading
    [SerializeField] private string LevelName;

    //Variable/Reference To Keep Track Of Level Progress
    public static bool finished;
    public static bool won;

    //Variables/References To Keep Track Of Stats
    public static float storedTime = 0;
    public static float storedScore = 0;

    //----------------------------------------

    // Start is called before the first frame update
    private void Start()
    {
        finished = false;
        won = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))//If Player Touches Object
        {
            if(!finished)//If Player Has Not Already Won The Level
            {
                PlayerController.hasMoved = false;//To Stop The Clock
                finished = true;
                if (LevelName.Equals("WinScreen"))
                    won = true;

                storedTime = GameManager.Instance.levelTime;//Stores Current Time To Be Restored If Level Is Restarted
                storedScore = GameManager.Instance.score;//Stores Current Score To Be Restored If Level Is Restarted

                Debug.Log("Win!");

                SceneLoader.LoadScene(LevelName);//Loads New Scene
            }
        }
    }
}
