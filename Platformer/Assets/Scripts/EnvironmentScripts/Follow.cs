﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    //---------------References---------------

    public Transform target;

    //----------------------------------------

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            if (target.position.y >= transform.position.y + 8)//If Target Is 8 Or More Units Above Object
                transform.position = new Vector3(transform.position.x, target.position.y - 8, transform.position.z);//Moves Object Up 8 Units
            else//If Not
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);//Keeps Objects Current Position

        }
    }
}
