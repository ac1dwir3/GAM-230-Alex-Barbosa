﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelReset : MonoBehaviour
{
    //---------------References---------------

    //Variable/Reference For Scene Loading
    [SerializeField] private string currentLevel;

    //----------------------------------------

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))//If Player Touches Object
        {
            Destroy(other.gameObject);//Destroy Player
            PlayerController.isAlive = false;//Stops The Timer

            GameManager.Instance.levelTime = Finish.storedTime;//Resets Timer To Time Stored At End of Previous Level
            GameManager.Instance.score = Finish.storedScore;//Resets Score To Score Stored At End of Previoous Level

            Debug.Log("You Lost!!");
            SceneLoader.LoadScene(currentLevel);//Reloads Current Scene
        }
    }
}
