﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObstacle : MonoBehaviour
{
    //---------------References---------------

    //Variables/References For Positions
    public Transform position1;
    public Transform position2;

    //Variables/References For Movement
    private float t = 0.0f;
    public float timePeriod = 1.0f;//For How Long You Want Movement between Postions To Be
    private float direction = 1.0f;//Direction Object Is Moving (1 = Position 1 To Position 2; -1 = Position 2 To Position 1)

    //----------------------------------------

    // Update is called once per frame
    void Update()
    {
        if(position1 != null && position2 != null)//If Object Was Both Given Positions
        {
            transform.position = Vector3.Lerp(position1.position, position2.position, t);//Move Object Smoothly From One Postion To Another

            t += direction * Time.deltaTime/timePeriod;//Keeps Track Of How Much Time To Destination

            if (t <= 0 || t >= 1)//If t Gets Below 0 OR above 1
                direction = -direction;//Change Movement Direction Of Object To Opposite Direction
        }
    }
}
