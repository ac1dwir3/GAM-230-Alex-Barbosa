﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKiller : MonoBehaviour
{
    //---------------References---------------

    //Variables/References To Keep Track Of Stats
    public static float storedTime = 0;
    public static float storedScore = 0;

    //----------------------------------------

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))//If Player Touches Object
        {
            Destroy(other.gameObject);//Destroy Player
            PlayerController.isAlive = false;//Stops The Timer

            storedTime = GameManager.Instance.levelTime;//Stores Current Time To Be Displayed At Lose Screen
            storedScore = GameManager.Instance.score;//Stores Current Score To Be Displayed At Lose Screen

            Debug.Log("You Lost!!");
            GameManager.Lose();//Loads Lose Screen Scene
        }
    }
}
