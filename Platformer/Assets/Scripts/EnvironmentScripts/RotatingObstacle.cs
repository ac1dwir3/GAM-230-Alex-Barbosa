﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingObstacle : MonoBehaviour
{
    //---------------References---------------

    //References Of Rotations
    Quaternion startingRotation;
    Quaternion targetRotation;

    //Variables/References For Rotation
    [SerializeField] float turnDegrees = 180f;//Allows To Set Individual Rotations For Each Object (Default = 180 Degrees)
    private static float degrees;//Needed For Rotation Code Later

    //Variable/Reference For Rotation Logic
    bool hasFlipped = false;

    //----------------------------------------

    // Start is called before the first frame update
    private void Start()
    {
        startingRotation = transform.rotation;//Saves Rotation The Object Starts In

        degrees = turnDegrees;//Saves Value Set By Player To Use In Rotation Code
        targetRotation = Quaternion.Euler(transform.rotation.eulerAngles.z, transform.rotation.eulerAngles.y, degrees);//Creates Desired End Rotation
    }

    //Rotates An Object Smoothly
    IEnumerator Rotate(Quaternion originalRotation, Quaternion finalRotation, float flipTime)//Takes In 2 Rotations And A Total Time
    {
        float rotationCompleted = 0f;
        while (rotationCompleted <= flipTime)
        {
            rotationCompleted += Time.deltaTime;//Keeps Track Of How Much Time To Rotate Is Remaining
            float percentLeft = Mathf.Clamp01(rotationCompleted / flipTime);//Keeps Track Of How Much Rotation Is Remaining

            transform.rotation = Quaternion.Lerp(originalRotation, finalRotation, percentLeft);//Rotates Object Smoothly
            yield return null;
        }

        hasFlipped = true;
    }

    //Instructions Of Desired Flip/Rotation
    IEnumerator Flip()
    {
        yield return new WaitForSeconds(.2f);//Waits 0.2 Seconds
        StartCoroutine(Rotate(startingRotation, targetRotation, .5f));//Rotates Object To New Rotation In 0.5 Seconds
        yield return new WaitForSeconds(2);//Waits For 2 Seconds In New Rotation
        StartCoroutine(Rotate(targetRotation, startingRotation, .5f));//Rotates Back To Starting Rotation In 0.5 Seconds
        yield return new WaitForSeconds(2);//Waits For 2 Seconds
        hasFlipped = false;//Resets Itself Allowing For Another Rotation If Restarted
    }

    private void OnTriggerStay(Collider other)//If Another Object Stays On The Rotational Object
    {
        if (other.CompareTag("Player") && !hasFlipped)//If Player Touches Object AND The Object Isn't Already Rotating
        {
            StartCoroutine(Flip());//Starts Rotation Instructions
        }
    }
}
