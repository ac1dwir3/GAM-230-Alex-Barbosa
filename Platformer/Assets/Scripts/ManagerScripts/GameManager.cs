﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    //---------------References---------------

    //Variables/References To Ensure Only One Exists
    private static GameManager _instance = null;
    public static GameManager Instance { get { return _instance; } }

    //Varables/References For Progression Stats
    public float score;
    public float levelTime;

    //----------------------------------------

    void Awake()
    {
        //Makes Sure There Is Only 1 Instance Of The GameManager Object
        if (_instance == null)//If The Is No GameManager Objects Currently In The Scene
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);//Makes Sure The GameManager Object Persists Throughout Entire Game
        }
        else//If More Than One
            Destroy(gameObject);//Destroy Extra
    }

    // Start is called before the first frame update
    private void Start()
    {
        score = 0;
        levelTime = 0;
    }

    // Update is called once per frame
    private void Update()
    {
        if(PlayerController.hasMoved && !Finish.finished && PlayerController.isAlive && !Finish.won)//If Player Has Moved AND Player Hasn't Yet Finished The Level AND If The Player Is Currently Alive AND If The Player Hasn't Yet Reached The Win Screen
        {
            levelTime += Time.deltaTime;//Increase Timer
        }
    }

    public static void Win()
    {
        SceneLoader.LoadScene("WinScreen");//Loads Win Screen Scene
    }

    public static void Lose()
    {
        SceneLoader.LoadScene("LoseScreen");//Loads Lose Screen Scene
    }
}
