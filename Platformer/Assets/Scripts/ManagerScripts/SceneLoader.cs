﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void SceneToLoad(string sceneName)
    {
        LoadScene(sceneName);
    }

    public static void LoadScene(string newScene)
    {
        Debug.Log("Loading Scene...");
        SceneManager.LoadScene(newScene);//Loads New Scene

        PlayerController.hasMoved = false;//Stops Timer
        Finish.finished = false;//Prevents Calling This Function Multiple Times If Player Moves While On The Finish Platform

        if (newScene.Equals("Level1"))//If Loading Level 1
        {
            Debug.Log("Resetting Score And Timer...");
            GameManager.Instance.levelTime = 0;//Resets Timer To 0
            GameManager.Instance.score = 0;//Resets Score To 0
        }
    }

    public void QuitGame()
    {
        Debug.Log("Quitting Game");
        Application.Quit();
    }
}
