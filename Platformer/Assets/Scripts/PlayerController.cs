﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //---------------References---------------

    [SerializeField] private CharacterController controller;
    [SerializeField] private Transform cam;
    [SerializeField] private AudioSource sound;

    //Variables/References For Player Movement
    [SerializeField] private float movementSpeed = 10.0f;
    [SerializeField] private float jumpHeight = 3.0f;
    [SerializeField] private float doubleJumpMultiplier = 0.75f;

    //Variables/References For Turning To Face Camera Direction
    private float turnSmoothTime = 0.1f;
    private float turnSmoothVelocity;

    //Variables/References For Falling/Jumping
    Vector3 velocity;
    [SerializeField] private float gravity = -9.81f*2f;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private float groundDistance = 0.8f;
    [SerializeField] private LayerMask groundMask;
    private bool isGrounded = false;
    private bool canDoubleJump = false;
    [SerializeField] private AudioClip jumpSound;

    //Variable/Reference For Level Timer
    public static bool hasMoved;
    public static bool isAlive;

    //----------------------------------------

    // Start is called before the first frame update
    private void Start()
    {
        hasMoved = false;//Prevents Timer From Starting When Loading Into New Level After Already Moving In Previous Level
        isAlive = true;//Prevents Timer From Starting When Loading Into New Level After Already Moving In Previous Level
    }

    // Update is called once per frame
    private void Update()
    {
        //Checks If Player Is On The Ground
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        if (isGrounded && velocity.y <= 0)//If On Ground AND Not Moving Up
        {
            velocity.y = -5f;//Sets Downward Velocity To Small Number To Make Sure The Player Is Completely Grounded
        }

        //Gets Player Inputs For Movement
        float horiz = Input.GetAxisRaw("Horizontal");
        float vert = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horiz, 0, vert).normalized;

        if(direction.magnitude >= 0.1f)//If Moving
        {
            hasMoved = true;

            //Rotates Player To Face Direction Input Values Are Pointing
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;//Determines Angle to Rotate Player
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);//Smooths Rotation So Player Is Not Snapping Into Position
            transform.rotation = Quaternion.Euler(0, angle, 0);//Rotates Player Smoothly

            //Moves Player
            Vector3 moveDirection = Quaternion.Euler(0, targetAngle, 0) * Vector3.forward;//Determines Direction Of Input
            controller.Move(moveDirection.normalized * movementSpeed * Time.deltaTime);//Moves Player
            
        }

        //Checks If Player Can Jump
        if (isGrounded)//If On The Ground
        {
            canDoubleJump = true;//Has Second Jump Stored
            if(Input.GetButtonDown("Jump"))//If Spacebar Is Pressed
            {
                velocity.y = JumpForce();//Applys Jump Force
                sound.clip = jumpSound;//Sets Audio To Jump Sounds
                sound.Play();//Plays Jump Sound
            }
        }
        else//If Not On The Ground
        {
            if(Input.GetButtonDown("Jump") && canDoubleJump)//If Spacebar Was Pressed AND Player Has Double Jump Stored
            {
                velocity.y = JumpForce() * doubleJumpMultiplier;// Double Jump Is A Smaller/Larger Jump Than Normal Jump Depending On Multiplier
                sound.clip = jumpSound;//Sets Audio To Jump Sounds
                sound.Play();//Plays Jump Sound
                canDoubleJump = false;//Has Used Second Jump. No More Jumps Until Player Hits Ground
            }
        }

        //Applys Gravity To Player
        velocity.y += gravity * Time.deltaTime;//Calculates Gravity
        controller.Move(velocity * Time.deltaTime);//Applys Force To Player Based On Freefall Physics Equation (Accelerates Downwards Over Time)
    }

    private float JumpForce()
    {
        return Mathf.Sqrt(jumpHeight * -2 * gravity);//Calculates Jump Force Based On Jumping Physics Equation
    }

}
