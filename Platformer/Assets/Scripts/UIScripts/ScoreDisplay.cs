﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreDisplay : MonoBehaviour
{
    //---------------References---------------

    //Variables/References For Displaying Score
    float score = 0;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private string preText;

    //----------------------------------------

    // Update is called once per frame
    private void Update()
    {
        score = GameManager.Instance.score;//Gets Current Score

        scoreText.text = preText + string.Format("{0:0}", score);//Displays Score
    }
}
