﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TimerDisplay : MonoBehaviour
{
    //---------------References---------------

    //Variables/Refernces For Displaying Time
    float time = 0;
    [SerializeField] private TextMeshProUGUI timeText;
    [SerializeField] private string preText;

    //----------------------------------------

    // Update is called once per frame
    private void Update()
    {
        time = GameManager.Instance.levelTime;//Gets Current Level Time

        timeText.text = preText + string.Format("{0:f2}", time);//Changes Timer Text To Current Level Time
    }
}
