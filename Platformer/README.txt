TITLE OF GAME: Platformer
CREATED BY: Alex Barbosa

CONTROLS:
W = Move Forward
A = Move Left
S = Move Backward
D = Move Right
SPACEBAR = Jump
SPACEBAR IN MID-AIR = Double Jump (Maximum Of 1 Mid-Air Jump)
MOUSE= Look Around

COLOR KEY:
WHITE OBJECTS = Normal Floor
RED OBJECTS = Kills Player (Lose Screen)
BLUE OBJECTS = Sinks Player (Player Falls Through BUT Can Still Jump When Sinking)
PURPLE OBJECTS = Trap Door (Swings Out From Underneath Player)
YELLOW PLATFORMS = Flips Over
GREEN OBJECTS = Finish Line (Starts Next Level/Win Screen)

GOAL: Collect Spinning Coins To Increase Score AND Reach The Finish Lines As Fast As You Can.

DISCLAIMER: Maps Are Abridged Adaptations Of The Speed Run Levels Found In Astro's Playroom For Playstation5.