using UnityEngine;

public class Collectible : MonoBehaviour
{
    [SerializeField] private float spinSpeed = 50;
    public bool collected = false;

    // Start is called before the first frame update
    void Start()
    {
        LevelManager.Instance.totalCollectibles++; //Increases Amount Of Collectibles In Current Level
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(transform.position, new Vector3(0, 1, 0), spinSpeed * Time.deltaTime); //Rotates Collectible

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) //If Player Touches The Collectible
        {
            collected = true;
            gameObject.SetActive(false); //Disables Self
        }
    }

    private void OnDisable()
    {
        if(collected)
        {
            GameManager.Instance.score += 50; //Increases Score
            LevelManager.Instance.collectiblesCollected++; //Increase Amount Of Collectibles Collected In The Level
            AudioManager.Instance.Play("Collectible Pick Up"); //Plays Sound
            Destroy(gameObject, AudioManager.Instance.GetAudioClip("Collectible Pick Up").length); //Destroys Self After Sound Finishes Playing
        }
    }
}
