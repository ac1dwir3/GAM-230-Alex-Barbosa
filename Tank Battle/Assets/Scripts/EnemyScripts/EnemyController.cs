using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyController : MonoBehaviour
{
    //Variables For Movement
    [SerializeField] private bool stationary = false;
    private NavMeshAgent agent;
    [SerializeField] private float movementSpeed = 1f;
    //For Patroling
    private Vector3 walkPoint;
    private bool walkPointSet;
    [SerializeField] private float walkPointRange;
    [SerializeField] private LayerMask groundLayer;
    //For Chasing Target
    private EnemyFOV vision;

    private void Awake()
    {
        vision = GetComponent<EnemyFOV>();
        agent = GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    void Start()
    {
        LevelManager.Instance.totalEnemies++; //Increase Count Of Total Enemies In Current Level
        agent.speed = movementSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if(!stationary) //If Not Stationary Enemy
        {
            if (!vision.canSeeTarget)
                Patrol();
            else
                ChaseTarget();
        }
    }

    void Patrol()
    {
        if (!walkPointSet)
            SearchWalkPoint();

        else if (walkPointSet)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;
        if (distanceToWalkPoint.magnitude < 1f + agent.stoppingDistance) //WalkPoint Reached
            walkPointSet = false;

    }
    void SearchWalkPoint()
    {
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ); //Creates Random Destination Nearby

        if (Physics.Raycast(walkPoint, -transform.up, 2f, groundLayer.value)) //If Random Destination Chosen Is On The Ground
            walkPointSet = true; //Random Destination Is Valid
    }

    void ChaseTarget()
    {
        agent.SetDestination(vision.target.position);
        GetComponentInChildren<EnemyShooting>().Attack(); //Shoots At Target While Chasing
    }
}
