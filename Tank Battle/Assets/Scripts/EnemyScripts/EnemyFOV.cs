using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFOV : MonoBehaviour
{
    //Variables For Field Of View
    [SerializeField] public float radius;
    [Range(0, 360)] [SerializeField] public float angle;

    public GameObject targetRef;
    [HideInInspector] public Transform target;

    [HideInInspector] public bool canSeeTarget;

    //Variables For Finding Target
    [SerializeField] private LayerMask targetMask;
    [SerializeField] private LayerMask obstructionMask;


    //Variables For Rotating
    private Vector3 directionToTarget;
    [SerializeField] private float turnSmoothTime = 0.5f;
    private float turnSmoothVelocity;


    // Start is called before the first frame update
    void Start()
    {
        targetRef = GameObject.FindGameObjectWithTag("Player");

        StartCoroutine(FOVRoutine());
    }

    // Update is called once per frame
    void Update()
    {
        if(canSeeTarget && directionToTarget != null) //If Target Is In Sight
        {
            float targetAngle = Mathf.Atan2(directionToTarget.x, directionToTarget.z) * Mathf.Rad2Deg; //Finds Angle To Look Towards
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime); //Smoothes Rotation
            transform.rotation = Quaternion.Euler(0, angle, 0); //Rotates To Face Target
        }
    }

    private IEnumerator FOVRoutine()
    {
        float delay = 0.2f;

        while (true)
        {
            yield return new WaitForSeconds(delay);
            FOVCheck();
        }
    }

    private void FOVCheck()
    {
        Collider[] rangeChecks = Physics.OverlapSphere(transform.position, radius, targetMask); //Checks For Target In Specified Radius

        if(rangeChecks.Length != 0) //If A Target Is Found
        {
            target = rangeChecks[0].transform; //First Index Is Most Likely The Desired Target (Not Fool-Proof If Set Up Incorrectly)
            directionToTarget = (target.position - transform.position).normalized;

            if(Vector3.Angle(transform.forward, directionToTarget) < angle / 2) //If Target Is In Field Of View
            {
                float distanceToTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics.Raycast(transform.position, directionToTarget, distanceToTarget)) //If Target Is Not Obscured
                    canSeeTarget = true;
                else //If Target Is Obcured
                    canSeeTarget = false;
            }
            else //If Target Is Not In Field Of View
                canSeeTarget = false;
        }
        else if (canSeeTarget) //If Target Was Found But Has Left The Field Of View Or Has Become Obscured
            canSeeTarget = false;
    }
}
