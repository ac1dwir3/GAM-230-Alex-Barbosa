using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    //Variables For Health/Death
    [SerializeField] public int health = 1;
    private bool defeated = false;
    [SerializeField] private GameObject tankDebris;
    [SerializeField] private bool backWeakness = false;

    // Start is called before the first frame update
    void Start()
    {
        if (health < 1)
            health = 1;
    }

    // Update is called once per frame
    void Update()
    {
        //Dying
        if (health <= 0)
        {
            health = 0;
            defeated = true;
            gameObject.SetActive(false);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        string colliderTag = collision.collider.tag;

        switch (colliderTag) //Checks To See What It Hit
        {
            case "Enemy Projectile": //Takes Damage From Friendly Fire
                if (!backWeakness) //If No Weakness On Back
                    health--;
                else //If Has Weakness On Back
                {
                    if (CheckAlignment(transform.forward, collision.gameObject.transform.forward) >= 0.8f) //If Projectile Hit Enemy's Back
                        health--;
                    else //If Projectile Hit Anywhere Other Than Enemy's Back
                        break;
                }
                break;
            case "Collectible": //Ignore Collectibles To Pass Right Through Them
                Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider, true);
                break;
            case "Player Projectile": //Takes Damage
                if (!backWeakness) //If No Weakness On Back
                    health--;
                else //If Has Weakness On Back
                {
                    if (CheckAlignment(transform.forward, collision.gameObject.transform.forward) >= 0.8f) //If Projectile Hit Enemy's Back
                        health--;
                    else //If Projectile Hit Anywhere Other Than Enemy's Back
                        break;
                }
                break;
            default:
                break;
        }
    }
    private float CheckAlignment(Vector3 lookDir, Vector3 otherLookDir)
    {
        return Vector3.Dot(lookDir.normalized, otherLookDir.normalized);
    }

    private void OnDisable()
    {
        if (defeated)
        {
            GameManager.Instance.score += 100; //Increases Score
            LevelManager.Instance.enemiesDefeated++; //Increases Number Of Defeated Enemies In Current Level
            Instantiate(tankDebris, transform.position, transform.rotation); //Spawns Tank Wreckage
        }

        Destroy(gameObject);//Destroys Self
    }
}
