using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyShooting : MonoBehaviour
{
    //Variables For Shooting
    private float fireRate = 2;
    [SerializeField] private GameObject projectile;
    [SerializeField] private Transform gun;
    private bool fired = false;

    private EnemyFOV vision;

    private void Awake()
    {
        vision = GetComponentInParent<EnemyFOV>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (vision.canSeeTarget && !fired) //If Target In View And Haven't Already Fired
        {
            Attack();
        }
    }

    public void Attack()
    {
        if(!fired)
        {
            Instantiate(projectile, gun.position, gun.rotation); //Spawns Projectile

            fired = true;
            Invoke(nameof(ResetAttack), fireRate); //Allows To Shoot Again After fireRate (2) Amount Of Seconds
        }
    }

    private void ResetAttack()
    {
        fired = false;
    }
}
