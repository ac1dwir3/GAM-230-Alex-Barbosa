using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private float spawnDelay;
    private bool spawning = false;
    [SerializeField] private int maxEnemies;
    

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnEnemies());
    }

    IEnumerator SpawnEnemies()
    {
        yield return new WaitForSeconds(spawnDelay);

        for (int i = 0; i < maxEnemies; i++)
        {
            if (!spawning)
            {
                spawning = true;
                Instantiate(enemyPrefab, transform.position, transform.rotation);
                yield return new WaitForSeconds(spawnDelay);
            }
            spawning = false;
        }
    }
}
