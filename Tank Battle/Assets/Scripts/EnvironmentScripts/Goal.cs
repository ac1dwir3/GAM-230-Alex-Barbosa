using System.Collections;
using UnityEngine;
using TMPro;

public class Goal : MonoBehaviour
{
    GameManager gm;
    LevelManager lm;

    [SerializeField] private TextMeshProUGUI FinishText;

    // Start is called before the first frame update
    void Start()
    {
        gm = GameManager.Instance;
        lm = LevelManager.Instance;

        FinishText.gameObject.SetActive(false);
    }
    
    IEnumerator FinishLevel()
    {
        gm.currentLives++; //Add A Life (1-Up After Level Completion)
        if (gm.currentLives > gm.maxPlayerLives)
            gm.maxPlayerLives++; // Increases Max Player Health If Current Health Is More That Current Max
        if(500 - lm.levelTimer >= 0) //Never Deduct From Overall Score If Player Took Too Long To Reach Goal
            gm.score += 500 - lm.levelTimer; //500 Points For Reaching Goal But Points Deducted The Longer It Takes

        Time.timeScale = 0;
        FinishText.gameObject.SetActive(true);

        yield return new WaitForSecondsRealtime(3);

        SceneLoader.Instance.NextScene(); //Loads Next Scene In Build
        Time.timeScale = 1f;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")) //If Player Touches The Goal
        {
            StartCoroutine(FinishLevel());
        }
    }

    private void OnEnable()
    {
        if(lm != null && lm.goalMet)
            AudioManager.Instance.Play("Goal"); //Plays Sound
    }
}
