using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    private static AudioManager _instance = null;
    public static AudioManager Instance { get { return _instance; } }

    public Sound[] sounds;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
        //--------------------------------------------------

        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.mute = s.mute;
            s.source.spatialBlend = s.spacialBlend;
        }
    }

    public void Play(string clipName)
    {
        Sound s = GetSound(clipName);
        if(s == null)//If Sound Not Found
        {
            Debug.LogWarning("Cannot Locate Sound Named: " + clipName); //Displays Warning
            return; //Quit Process
        }

        s.source.Play(); //Plays Sound
    }

    // Start is called before the first frame update
    void Start()
    {
        Play("Theme Music");
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.isPaused)
        {
            for (int i = 0; i < sounds.Length; i++)
                sounds[i].source.pitch *= .5f;
        }
        else
        {
            for (int i = 0; i < sounds.Length; i++)
                sounds[i].source.pitch = 1f;
        }
    }

    public AudioClip GetAudioClip(string clipName)
    {
        Sound s = GetSound(clipName);
        if (s == null) //If Sound Not Found
        {
            Debug.LogWarning("Cannot Locate Sound Named: " + clipName);
            return null; //Quits Process
        }

        return s.source.clip; //Returns Sound
    }

    public Sound GetSound(string clipName)
    {
        return Array.Find(sounds, sound => sound.name == clipName); //Searches For Sound In Sounds Container
    }
}

[System.Serializable]
public class Sound
{
    //Variables For Sounds
    public string name;
    public AudioClip clip;

    //Variables For Sound Adjustments
    public bool mute;
    public bool loop;

    [Range(0f, 1f)]
    public float volume;
    [Range(.1f, 3f)]
    public float pitch;
    [Range(0f, 1f)]
    [Tooltip("Determines whether the sound is 2D or 3D. 2D sounds can be heard from anywhere. " +
        "3D sounds are quieter the farther the AudioListener is from the sound's origin (0 = 2D, 1 = 3D)")]
    public float spacialBlend;

    [HideInInspector]
    public AudioSource source;
}
