using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironManager : MonoBehaviour
{
    private static EnvironManager _instance = null;
    public static EnvironManager Instance { get { return _instance; } }


    //Variables To Keep Track Of Per Scene
    private LevelManager lm;
    [SerializeField] private GameObject[] goalObjects;
    

    void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        lm = FindObjectOfType<LevelManager>();

        if (goalObjects.Length == 0)
            goalObjects[0] = GameObject.Find("Goal");

        foreach(GameObject goal in goalObjects)
            goal.SetActive(false); //Hides Finish Platforms
    }

    // Update is called once per frame
    void Update()
    {
        if (lm.needBothGoalsComplete && !(lm.collectibleGoal == 0 || lm.enemyGoal == 0)) //If You Need To Collect Collectibles AND Defeat Enemies To Win Level
        {
            if(lm.collectiblesCollected == lm.collectibleGoal && lm.enemiesDefeated == lm.enemyGoal)
            {
                lm.goalMet = true;
                foreach (GameObject goal in goalObjects)
                    goal.SetActive(true); //Unhides Finish Platforms
            }
        }
        else if (lm.needCollectibleGoal && lm.needEnemyGoal && !(lm.collectibleGoal == 0 || lm.enemyGoal == 0)) //If You Need To Collect Collectibles OR Defeat Enemies To Win Level
        {
            if (lm.collectiblesCollected == lm.collectibleGoal || lm.enemiesDefeated == lm.enemyGoal)
            {
                lm.goalMet = true;
                foreach (GameObject goal in goalObjects)
                    goal.SetActive(true); //Unhides Finish Platforms
            }
        }
        else if(lm.needCollectibleGoal && lm.collectibleGoal != 0) //If You ONLY Need To Collect Collectibles To Win Level
        {
            if (lm.collectiblesCollected == lm.collectibleGoal)
            {
                lm.goalMet = true;
                foreach (GameObject goal in goalObjects)
                    goal.SetActive(true); //Unhides Finish Platforms
            }
        }
        else if(lm.needEnemyGoal && lm.enemyGoal != 0) //If You ONLY Need To Defeat Enemies To Win Level
        {
            if(lm.enemiesDefeated == lm.enemyGoal)
            {
                lm.goalMet = true;
                foreach (GameObject goal in goalObjects)
                    goal.SetActive(true); //Unhides Finish Platforms
            }
        }
        else //If No Requirment For Goal Needed
        {
            lm.goalMet = true;
            foreach (GameObject goal in goalObjects)
                goal.SetActive(true); //Unhides Finish Platforms
        }
    }
}
