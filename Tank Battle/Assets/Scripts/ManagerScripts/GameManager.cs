using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    public static GameManager Instance { get { return _instance; } }

    //Variables To Keep track Of Throughout Entire Game
    [HideInInspector] public float score = 0;

    //Variables For Player
    [HideInInspector] public int maxPlayerLives = 3;
    [HideInInspector] public int maxPlayerAmmo = 3;
    [HideInInspector] public int currentLives;
    [HideInInspector] public int currentAmmo;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        currentLives = maxPlayerLives; //Starts Player With Full Health
        currentAmmo = maxPlayerAmmo; //Starts Player With Max Ammo
    }
}
