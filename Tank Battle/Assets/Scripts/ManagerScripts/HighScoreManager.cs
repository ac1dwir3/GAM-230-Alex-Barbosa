using System.Collections.Generic;
using UnityEngine;
using System.IO;

public struct HighScore
{
    public string name;
    public int score;

    public HighScore(string newName, int newScore)
    {
        name = newName;
        score = newScore;
    }
}

public class HighScoreManager : MonoBehaviour
{
    public List<HighScore> scores = new List<HighScore>();

    private void Awake()
    {
        Load();
    }

    public void Save()
    {
        Debug.Log("Persistant Data Path: " + Application.persistentDataPath);

        string saveFile = Application.persistentDataPath + "/HighScores.txt";

        StreamWriter writer = new StreamWriter(saveFile);

        for(int i = 0; i<scores.Count; i++)
        {
            writer.WriteLine(scores[i].name + "|" + scores[i].score);
        }

        writer.Close();
    }

    public void Load()
    {
        //scores.Add(new HighScore("Alex", 1000));
        //scores.Add(new HighScore("Michael", 500));
        //scores.Add(new HighScore("Brooke", 10));

        string saveFile = Application.persistentDataPath + "/HighScores.txt";

        StreamReader reader = new StreamReader(saveFile);

        scores.Clear();

        while(reader.Peek() > -1)
        {
            string s = reader.ReadLine();
            if (s.Length == 0)
                continue;

            int index = s.LastIndexOf('|');
            if(index == -1)
            {
                //Malformed High Score: ignore
                continue;
            }

            HighScore hs = new HighScore();
            hs.name = s.Substring(0, index);
            if (int.TryParse(s.Substring(index + 1, s.Length - (index + 1)), out hs.score))
            {
                scores.Add(hs);
            }
        }

        reader.Close();
    }
}
