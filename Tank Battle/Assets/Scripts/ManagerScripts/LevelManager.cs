using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LevelManager : MonoBehaviour
{
    private static LevelManager _instance = null;
    public static LevelManager Instance { get { return _instance; } }

    private GameManager gm;

    //Variables To Keep Track Of Per Level
    [HideInInspector] public int totalCollectibles;
    [HideInInspector] public int collectiblesCollected;
    [HideInInspector] public int totalEnemies;
    [SerializeField] public int maxEnemies;
    [HideInInspector] public int enemiesDefeated;
    [HideInInspector] public bool goalMet = false;
    [HideInInspector] public float levelTimer = 0f;

    //Variables For Activating Goal;
    [SerializeField] public bool needEnemyGoal;
    [SerializeField] public bool needCollectibleGoal;
    [SerializeField] public bool needBothGoalsComplete;
    [SerializeField] public int enemyGoal;
    [SerializeField] public int collectibleGoal;
    private bool goalSet = false;

    public GameObject player;

    public float currentScore;

    void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        gm = GameManager.Instance;
        currentScore = gm.score;

        player = GameObject.Find("Player");

        
    }

    // Update is called once per frame
    void Update()
    {
        if (totalEnemies > maxEnemies)
            maxEnemies = totalEnemies;

        if(!goalSet)
        {
            if (needEnemyGoal && enemyGoal == 0)
            {
                enemyGoal = totalEnemies;
                goalSet = true;
            }
            if (needCollectibleGoal && collectibleGoal == 0)
            {
                collectibleGoal = totalCollectibles;
                goalSet = true;
            }
            if (needBothGoalsComplete && (collectibleGoal == 0 || enemiesDefeated == 0))
            {
                if (needEnemyGoal && enemyGoal == 0)
                {
                    enemyGoal = totalEnemies;
                    goalSet = true;
                }
                if (needCollectibleGoal && collectibleGoal == 0)
                {
                    collectibleGoal = totalCollectibles;
                    goalSet = true;
                }
            }
        }

        levelTimer += Time.deltaTime;
    }
}
