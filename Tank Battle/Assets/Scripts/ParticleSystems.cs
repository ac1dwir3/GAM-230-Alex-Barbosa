﻿using UnityEngine;

public class ParticleSystems : MonoBehaviour
{
    private AudioManager am;

    //Variables For Sounds
    [SerializeField] private string audioClipName;
    [SerializeField] private bool continuous = true;

    private void Start()
    {
        am = AudioManager.Instance;

        if (!continuous)
        {
            if (am.GetAudioClip(audioClipName)) //If Sound Name Was Provided And Is Found
            {
                am.Play(audioClipName); //Plays Sound
                Destroy(gameObject, am.GetAudioClip(audioClipName).length); //Destroys Self After Sound Is Finished Playing

            }
            else //If No Sound Provided Or Provided Sound Was Not Found
                Destroy(gameObject, 5f); //Destroys Self After 5 Seconds
        }
    }
}
