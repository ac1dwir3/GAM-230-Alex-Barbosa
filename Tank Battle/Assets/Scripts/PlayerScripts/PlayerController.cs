using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    //Variables For Moving
    private CharacterController controller;
    [SerializeField] private float movementSpeed = 2f;
    Vector3 velocity;
    private float turnSmoothTime = 0.5f;
    private float turnSmoothVelocity;

    //Variables For Aiming
    private float lookTurnSmoothTime = 0.5f;
    private float lookTurnSmoothVelocity;
    [SerializeField] private Transform turret;

    //Variables For Jumping
    [SerializeField] private float jumpHeight = 2f;
    [SerializeField] private GameObject jumpParticles;
    //Variables For Keeping Grounded
    private float gravity = -9.81f;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private float groundDistance = 0.4f;
    [SerializeField] private LayerMask groundMask;
    private bool isGrounded;

    //Variable To Prevent Health Bugs When Player Object Is Disabled/Destroyed While Loading Scenes
    public static bool dead;

    // Start is called before the first frame update
    void Start()
    {
        controller = this.GetComponent<CharacterController>();
        dead = false;
    }

    // Update is called once per frame
    void Update()
    {
        float horizLook = Input.GetAxis("Horizontal Look");
        float vertLook = Input.GetAxis("Vertical Look");
        Vector3 lookDirection = new Vector3(horizLook, 0f, vertLook).normalized; //Look Direction

        //For Aiming
        if(lookDirection.magnitude >= 0.1f)
        {
            float targetLookAngle = Mathf.Atan2(lookDirection.x, lookDirection.z) * Mathf.Rad2Deg; //Finds Angle To Look Towards
            float lookAngle = Mathf.SmoothDampAngle(turret.eulerAngles.y, targetLookAngle, ref lookTurnSmoothVelocity, lookTurnSmoothTime); //Smoothes Rotation
            turret.rotation = Quaternion.Euler(0, lookAngle, 0); //Rotates Turret To Look Direction
        }

        //For Jumping
        if (Input.GetKeyDown(KeyCode.E) && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity); //Jump Physics
            Instantiate(jumpParticles, transform.position, jumpParticles.transform.rotation, transform); //Jump
        }
    }

    private void FixedUpdate()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask); //Checks If On the Ground

        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");
        Vector3 direction = new Vector3(horiz, 0f, vert).normalized; //Movement Direction

        if (isGrounded && velocity.y < 0)
            velocity.y = -2f; //Pushes Player To The Ground
        
        if(direction.magnitude >= 0.1f) //If Moving
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg; //Finds Angle To Look Towards While Moving
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime); //Smoothes Rotation
            transform.rotation = Quaternion.Euler(0, angle, 0); //Rotates Player To Face Movement Direction

            controller.Move(direction * movementSpeed * Time.deltaTime); //Moves Player In Movement Direction
        }

        velocity.y += gravity * Time.deltaTime; //Pushes Player Downward When Not On Ground
        controller.Move(velocity * Time.deltaTime); //Gravity Always Pulling PLayer Down
    }

    private void OnCollisionEnter(Collision collision)
    {
        string colliderTag = collision.collider.tag;

        switch (colliderTag) //Checks To See What It Hit
        {
            case "Player Projectile": //Prevents Accidentally Shooting Self...
                Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider, true);
                break;
            case "Enemy Projectile": //Defeated, Lowers Health, And Disables Player Object
                dead = true;
                GameManager.Instance.currentLives--;
                gameObject.SetActive(false);
                break;
            default:
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        string otherTag = other.tag;

        switch (otherTag)
        {
            case "Respawn":
                dead = true;
                GameManager.Instance.currentLives--;
                gameObject.SetActive(false);
                break;
            default:
                break;
        }
    }

    private void OnDisable()
    {
        if (dead && GameManager.Instance.currentLives > 0) //If Dead But Still Have More Lives Left
        {
            GameManager.Instance.score = LevelManager.Instance.currentScore; //Reset Score To What It Was At Beginning Of The Level
            SceneLoader.Instance.ReloadScene(); //Restarts Level
        }
        else if (dead && GameManager.Instance.currentLives == 0) //If Dead But NO Lives Left
        {
            SceneLoader.Instance.LoadScene("LoseScreen");
        }
    }
}
