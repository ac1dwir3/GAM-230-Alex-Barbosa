using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerProjectile : MonoBehaviour
{
    [SerializeField] private GameObject fireParticles;
    [SerializeField] private GameObject destructionParticles;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate(fireParticles, transform.position, transform.rotation); //Spawns Particles
        GetComponent<Rigidbody>().AddForce(transform.forward * 25f, ForceMode.Impulse); //Makes Projectile Move
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        string colliderTag = collision.collider.tag;

        switch (colliderTag) //Checks To See What It Hit
        {
            case "Player Projectile": //Prevents Accidentally Destroying Player's Own Bullets...
                Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider, true);
                break;
            case "Collectible": //Ignore Collectibles To Pass Right Through Them
                Physics.IgnoreCollision(GetComponent<Collider>(), collision.collider, true);
                break;
            case "Enemy Projectile": //Destroys Enemy Projectiles Allowing To Block Incoming Fire
                collision.gameObject.SetActive(false);
                Destroy(collision.gameObject);
                break;
            default:
                break;
        }

        Instantiate(destructionParticles, transform.position, transform.rotation); //Spawns Particles

        Destroy(gameObject); //Destroys Self
    }
}
