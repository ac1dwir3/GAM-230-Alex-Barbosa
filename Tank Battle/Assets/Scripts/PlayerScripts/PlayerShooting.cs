using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    //Variables For Shooting
    [SerializeField] private Transform gun;
    [SerializeField] private GameObject projectile;

    [SerializeField] private float fireRate = 0.5f;
    private float fireCooldown;
    private bool readyToFire = true;

    //Variables For Reloading
    private int reloadTime = 1;
    public static bool reloading;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.currentAmmo = GameManager.Instance.maxPlayerAmmo;
        reloading = false;
    }

    // Update is called once per frame
    void Update()
    {
        fireCooldown -= Time.deltaTime;

        if (fireCooldown <= 0)
            readyToFire = true;
        else
            readyToFire = false;


        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(GameManager.Instance.currentAmmo > 0 && !reloading && readyToFire)
            {
                Instantiate(projectile, gun.position, gun.rotation); //Spawns Projectile When SpaceBar Is Pressed
                GameManager.Instance.currentAmmo--;
                fireCooldown = fireRate;
            }
            else if(!reloading && readyToFire)
            {
                StartCoroutine(Reload());
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
            if (GameManager.Instance.currentAmmo < GameManager.Instance.maxPlayerAmmo && !reloading)
                StartCoroutine(Reload());
    }

    IEnumerator Reload()
    {
        reloading = true;
        yield return new WaitForSeconds(reloadTime);
        GameManager.Instance.currentAmmo = GameManager.Instance.maxPlayerAmmo;
        reloading = false;
    }


}
