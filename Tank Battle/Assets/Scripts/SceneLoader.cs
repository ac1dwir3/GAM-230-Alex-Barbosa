﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private static SceneLoader _instance = null;
    public static SceneLoader Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    public static int GetSceneIndex() //Gets Current Scene's Build Index
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

    public void LoadScene(string newScene) //Loads Scene by Name
    {
        Debug.Log("Loading Scene: " + newScene + "...");
        SceneManager.LoadScene(newScene);
    }
    public void LoadScene(int index) //Loads Scene By Index
    {
        Debug.Log("Loading Scene of Index: " + index + "...");
        SceneManager.LoadScene(index);
    }
    public void NextScene() //Loads Next Scene Based On Current Scene Index
    {
        Debug.Log("Loading Next Scene...");
        LoadScene(GetSceneIndex() + 1);
    }
    public void ReloadScene() //Reloads Current Scene
    {
        LoadScene(GetSceneIndex());
        Debug.Log("Scene Releaded");
    }
}
