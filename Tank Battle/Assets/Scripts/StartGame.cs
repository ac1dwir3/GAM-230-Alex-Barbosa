using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StartGame : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI startText;
    [SerializeField] private TextMeshProUGUI countdownText;
    [SerializeField] private TextMeshProUGUI playText;

    float timer = 3;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Intro());
    }

    IEnumerator Intro()
    {
        Time.timeScale = 0f;
        startText.gameObject.SetActive(true);
        yield return new WaitForSecondsRealtime(2);
        startText.gameObject.SetActive(false);
        StartCoroutine(Countdown());

    }

    IEnumerator Countdown()
    {
        countdownText.gameObject.SetActive(true);
        yield return new WaitForSecondsRealtime(3);
        countdownText.gameObject.SetActive(false);
        yield return null;
        StartCoroutine(PlayRoutine());
    }

    IEnumerator PlayRoutine()
    {
        Time.timeScale = 1f;
        playText.gameObject.SetActive(true);
        yield return new WaitForSecondsRealtime(2);
        playText.gameObject.SetActive(false);
    }

    private void Update()
    {
        if(countdownText.IsActive())
        {
            timer -= Time.unscaledDeltaTime;
            countdownText.text = string.Format("{0:0}", timer);
        }
    }
}
