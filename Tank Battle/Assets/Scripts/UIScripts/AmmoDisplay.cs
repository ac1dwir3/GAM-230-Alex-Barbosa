using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AmmoDisplay : MonoBehaviour
{
    [SerializeField] private Slider slider;
    [SerializeField] private TextMeshProUGUI ammoNumber;
    [SerializeField] private TextMeshProUGUI reloadText;

    // Start is called before the first frame update
    void Start()
    {
        reloadText.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (slider.maxValue != GameManager.Instance.maxPlayerAmmo)
            slider.maxValue = GameManager.Instance.maxPlayerAmmo; //Sets Max Value Of Slider To Max Player Health (Decrease/Increase Ammo Bar Capacity)

        if (PlayerShooting.reloading) //If Reloading
            reloadText.enabled = true; //Displays Reloading Text
        else //If Not Reloading
            reloadText.enabled = false; //Hides Reloading Text


        ammoNumber.text = string.Format("{0:0}", GameManager.Instance.currentAmmo); //Updates Number Of Bullets In The Mag Displayed On Shell

        slider.value = GameManager.Instance.currentAmmo; //Updates Slider To Match Current Health (Decrease/Increase Ammo Bar Fill)
    }
}
