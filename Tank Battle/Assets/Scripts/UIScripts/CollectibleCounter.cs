using UnityEngine;
using TMPro;

public class CollectibleCounter : MonoBehaviour
{
    private LevelManager lm;
    [SerializeField] private TextMeshProUGUI CollectibleCounterText;
    [SerializeField] private string preText;

    // Start is called before the first frame update
    void Start()
    {
        lm = LevelManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        CollectibleCounterText.text = preText + string.Format("{0:0}", lm.totalCollectibles - lm.collectiblesCollected); //Updates Number Of Collectible Remaining In Level
    }
}
