using System.Collections.Generic;
using UnityEngine;
using TMPro;

//MOST OF THIS CODE IS NOT UNIQUE TO ME. THIS SCRIPT IS LARGLY A TUTORIAL
//MADE BY CODEMONKEY: https://youtu.be/iAbaqGYdnyI
//HOWEVER, I HAVE MODIFIED IT FROM THE ORIGINAL CODE TO SUIT MY GAME, SO IT'S NOT A CARBON COPY
public class HighScoreTable : MonoBehaviour
{
    [SerializeField] private Transform entryContainer;
    [SerializeField] private Transform entryTemplate;
    private List<Transform> highScoreEntryTransformList;
    [SerializeField] private int currentAmountOfHighScores;

    [SerializeField] private TMP_InputField nameInput;

    private void Awake()
    {
        if(entryContainer == null)
            entryContainer = transform.Find("HighScoreEntryContainer");
        if(entryTemplate == null)
            entryTemplate = entryContainer.Find("HighScoreEntryTemplate");


        entryTemplate.gameObject.SetActive(false);

        string jsonString = PlayerPrefs.GetString("HighScoreTable");
        HighScores highscores = JsonUtility.FromJson<HighScores>(jsonString);

        currentAmountOfHighScores = highscores.highScoreEntryList.Count;

        //Sort Entry List By Score (Highest To Lowest)
        for (int i = 0; i < highscores.highScoreEntryList.Count; i++)
        {
            for (int j = i; j < highscores.highScoreEntryList.Count; j++)
            {
                if(highscores.highScoreEntryList[j].score > highscores.highScoreEntryList[i].score)
                {
                    //Swap
                    HighScoreEntry temp = highscores.highScoreEntryList[i];
                    highscores.highScoreEntryList[i] = highscores.highScoreEntryList[j];
                    highscores.highScoreEntryList[j] = temp;
                }
            }
        }

        highScoreEntryTransformList = new List<Transform>();
        for(int i = 0; i < 5; i++) //Only Displays Top 5 High Scores
        {
            CreateHighScoreEntryTransform(highscores.highScoreEntryList[i], entryContainer, highScoreEntryTransformList);
        }
    }

    private void CreateHighScoreEntryTransform(HighScoreEntry entry, Transform container, List<Transform> transformList)
    {
        float templateHeight = 50f;
        Transform entryTransform = Instantiate(entryTemplate, container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
        entryTransform.gameObject.SetActive(true);

        int rank = transformList.Count + 1;
        string rankString;
        switch (rank)
        {
            default:
                rankString = rank + "TH";
                break;
            case 1:
                rankString = "1ST";
                break;
            case 2:
                rankString = "2ND";
                break;
            case 3:
                rankString = "3RD";
                break;
        }

        entryTransform.Find("PosText").GetComponent<TextMeshProUGUI>().text = rankString;
        entryTransform.Find("NameText").GetComponent<TextMeshProUGUI>().text = entry.name;
        entryTransform.Find("ScoreText").GetComponent<TextMeshProUGUI>().text = string.Format("{0:0}", entry.score);
        entryTransform.Find("Background").gameObject.SetActive(rank % 2 == 1);

        transformList.Add(entryTransform);
    }

    public void StorePlayerName()
    {
        string playerName = nameInput.textComponent.text;
        AddHighScoreEntry(playerName);
    }

    public void AddHighScoreEntry(string name)
    {
        //Create HighScore Entry
        HighScoreEntry highScoreEntry = new HighScoreEntry { score = GameManager.Instance.score, name = name };

        //Load Saved HighScores
        string jsonString = PlayerPrefs.GetString("HighScoreTable");
        HighScores highscores = JsonUtility.FromJson<HighScores>(jsonString);

        //Add New Entry To HighScores
        highscores.highScoreEntryList.Add(highScoreEntry);

        //Save Updated HighScores
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("HighScoreTable", json);
        PlayerPrefs.Save();

    }

    private class HighScores
    {
        public List<HighScoreEntry> highScoreEntryList;
    }

    [System.Serializable]
    private class HighScoreEntry
    {
        public float score;
        public string name;
    }

    private void Update()
    {
        string jsonString = PlayerPrefs.GetString("HighScoreTable");
        HighScores highscores = JsonUtility.FromJson<HighScores>(jsonString);

        if(highscores.highScoreEntryList.Count > currentAmountOfHighScores)
        {

            //Sort Entry List By Score (Highest To Lowest)
            for (int i = 0; i < highscores.highScoreEntryList.Count; i++)
            {
                for (int j = i; j < highscores.highScoreEntryList.Count; j++)
                {
                    if (highscores.highScoreEntryList[j].score > highscores.highScoreEntryList[i].score)
                    {
                        //Swap
                        HighScoreEntry temp = highscores.highScoreEntryList[i];
                        highscores.highScoreEntryList[i] = highscores.highScoreEntryList[j];
                        highscores.highScoreEntryList[j] = temp;
                    }
                }
            }

            highScoreEntryTransformList = new List<Transform>();
            for (int i = 0; i < 5; i++) //Only Displays Top 5 High Scores
            {
                CreateHighScoreEntryTransform(highscores.highScoreEntryList[i], entryContainer, highScoreEntryTransformList);
            }

            currentAmountOfHighScores = highscores.highScoreEntryList.Count;
        }
    }
}
