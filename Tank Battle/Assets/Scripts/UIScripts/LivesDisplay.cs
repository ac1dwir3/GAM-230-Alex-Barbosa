using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LivesDisplay : MonoBehaviour
{
    [SerializeField] private Slider slider;
    [SerializeField] private TextMeshProUGUI livesNumber;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(slider.maxValue != GameManager.Instance.maxPlayerLives)
            slider.maxValue = GameManager.Instance.maxPlayerLives; //Sets Max Value Of Slider To Max Player Health (Decrease/Increase Health Bar Capacity)

        livesNumber.text = string.Format("{0:0}", GameManager.Instance.currentLives); //Updates Number Of Lives Displayed On Heart

        slider.value = GameManager.Instance.currentLives; //Updates Slider To Match Current Health (Decrease/Increase Health Bar Fill)
    }
}
