using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuNavigation : MonoBehaviour
{
    SceneLoader sl;
    // Start is called before the first frame update
    void Start()
    {
        sl = SceneLoader.Instance;
    }

    public void LoadScene(string sceneName)
    {
        sl.LoadScene(sceneName);
    }
    public void LoadScene(int sceneIndex)
    {
        sl.LoadScene(sceneIndex);
    }
    public void NextScene()
    {
        sl.NextScene();
    }
    public void RestartGame()
    {
        GameManager.Instance.currentLives = 3;
        GameManager.Instance.score = 0f;
        sl.LoadScene(1);
    }
}
