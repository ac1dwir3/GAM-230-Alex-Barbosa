﻿using UnityEngine;
using TMPro;

public class ScoreDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private string preText;

    // Update is called once per frame
    private void Update()
    {
        scoreText.text = preText + string.Format("{0:0}", GameManager.Instance.score); //Displays Score
    }
}
