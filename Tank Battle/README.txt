Title: Tank Battle
By: Alex Barbosa

Collect All Coins (50 Points Each) In The Level OR Defeat All Enemies (Red Tanks) (100 Points Each) In The Level To Unlock The 
Finish Platform And Advance To The Next level (500 Points Minus The Time It Took To Reach The Goal).
3 Shots Before Reloading. Unlimited Reloading. 3 Lives (+1 For Each Level Completed).
_____________________________________________________________________________________________________________________________
Controls:

WASD - Move Player (Green Tank)
ARROW KEYS - Rotates Player Turret (Aim)
SPACEBAR - Shoots Projectiles (Reloads If Mag Is Empty)
R - Reloads (Only When Mag Is Not Already Full)
E - Rocket Propelled Jump

_____________________________________________________________________________________________________________________________
Audio Links:

Music - https://youtu.be/VCNskcGD1Oc (Personally Modified For Seamless(ish) Loop)
Goal - https://youtu.be/Ah0UJTxwAxg (0:51 - 0:53)
Coin - Created With BFXR
Big Explosion, Small Explosion, And Jump - Magic & Melee Customizable Sound Library 
(Small - Fire_Hit_2_S, Big - Fire_Explosion_3_S, Jump - Fire_Hit_1_S)
https://assetstore.unity.com/packages/audio/sound-fx/weapons/magic-melee-customizable-sound-library-176417

_____________________________________________________________________________________________________________________________
Other Imported Assets:

Skybox: Free HDR Skyboxes Pack (Skybox 4)
https://assetstore.unity.com/packages/2d/textures-materials/sky/free-hdr-skyboxes-pack-175525